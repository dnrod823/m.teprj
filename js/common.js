$(document).ready(function() {

    //gnb_wrap 열기
    $('#header .btn_menu').on('click',function(){
        $('#header nav').addClass('on'); 
        // $('#header .dimm').addClass('on'); 
    });
    
    //gnb_wrap 닫기
    $('#header nav .btn_close').on('click',function(){
        $('#header nav').removeClass('on');    
        // $('#header .dimm').removeClass('on');    
    });

    //depth2 열기
    $('.gnb>li>a').on('click',function(){
        $('.gnb>li>a').removeClass('active');
        $('.gnb .depth2_wrap').removeClass('on');

        if(!$(this).siblings('.depth2_wrap').hasClass('on')) {
            $(this).addClass('active');
            $(this).siblings('.gnb .depth2_wrap').addClass('on');
        } 
    });
    
    $(".page_select_wrap .page span").click(function(){
        $(".page_select_wrap .page span").removeClass("page_sel");
        $(this).attr("class","page_sel");
    });

    //슬라이더
    var mySwiper=new Swiper('.main_slider', {
        loop:true,
        speed:1000,
        autoplay: {
            delay: 3000
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        }
    });


    /**************** 제품 메뉴 인덱스 ****************/// 
    $(".products_info:gt(0)").hide();
    $(".products_info_menu li").on("click", function(e) {
        e.preventDefault();
        var index = $(this).index();

        $(this).addClass("active").siblings().removeClass("active");
        $(".products_info").eq(index).show().siblings().hide();
    });


    // var galleryThumbs = new Swiper('.gallery-thumbs', {
    //     spaceBetween: 10,
    //     slidesPerView: 5,
    //     freeMode: true,
    //     watchSlidesVisibility: true,
    //     watchSlidesProgress: true,
    //     direction:'vertical'
    //   });

      var galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 10,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        }
        // ,
        // thumbs: {
        //   swiper: galleryThumbs
        // }
      });

    /****************page_sel 선택****************/
    $(".page_select_wrap .page span").click(function(){
        $(".page_select_wrap .page span").removeClass("page_sel");
        $(this).attr("class","page_sel");
    });

});

/****************게시판 화살표 클릭****************/
function numMove(n){

    if(n=="next"){
        var pageNum = Number($(".page_sel").text())+1;
    }else{
        var pageNum = Number($(".page_sel").text())-1;
    }

    console.log(pageNum);

    if((Number($(".page_sel").text())!="1" && n!="next") || (Number($(".page_sel").text()!="10") && n!="prev")){
        $(".page_select_wrap .page span").removeClass("page_sel");
        $("#"+(pageNum) + " span").attr("class","page_sel");
    }
}